## portfolio website

[the code on github](https://github.com/sheminusminus/portfolio-multibrowser)

[live site](https://emkolar.ninja)

some of my work can be found here :)

instructions on how to launch the web vr version are in the readme.

## medean webapp code partials

[code on gitlab](https://gitlab.com/sheminusminus/mdn-partials)

[live webapp](https://app.medean.com)

i'm currently the lead frontend and mobile engineer for medean. these are some code samples from the webapp.

## exokit vr browser website

[live site](https://exokitbrowser.com)

this is a community/promo website built for a client. (completed in about 2 or 3 days of actual work).

## maptracks ios app

[code on gitlab](https://gitlab.com/maptracks/ios)

[video demo](https://gitlab.com/sheminusminus/projects-and-demos/blob/master/maptracksDemo.mov)

a side project for tracking your playlist history on a map, and seeing + playing the songs others listened to on routes nearby.

## coaster challenge site

[live site](http://coaster.soill.org)

this was built for the special olympics illinois a couple of years ago, but is still one of my favorites.

## apple-music-token-node (a tiny npm package)

[code on github](https://github.com/sheminusminus/apple-music-token-node)

[npm page](https://www.npmjs.com/package/apple-music-token-node)

## my only foray into making something blockchain related

[code on github](https://github.com/sheminusminus/js-blockchain)

a very simple, small implementation of a blockchain in js. written on a random evening just for the heck of it :)

it's not much, but it was fun to make!
