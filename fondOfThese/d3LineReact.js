import React from 'react';
import { isEqual } from 'lodash';
import { line, select, scaleLinear, curveCardinal } from 'd3';

import { classie } from 'web/utils';

import { generateScaleOfType, generateXDomain } from '../utils';

import propTypes from './prop-types';
import defaultProps from './default-props';

/**
 * D3Line
 * A line-chart line to plot data points.
 * Props should include `data`, an array of objects, i.e. { label, value }.
 * @extends React.Component
 */
class D3Line extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      line: null,
      x: null,
      y: null,
    };
    this.groupRef = null;
    this.svg = null;
    this.bindCallbacks();
  }

  bindCallbacks() {
    this.setGroupRef = this.setGroupRef.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { width, height, data } = this.props;
    const { width: nextWidth, height: nextHeight, data: nextData } = nextProps;
    // once all our components render and refs are set, the widths will update, and
    // thus our chart x-range will need updating
    if (width !== nextWidth || height !== nextHeight || !isEqual(data, nextData)) {
      this.makeScales(nextProps);
    }
  }

  setGroupRef(el) {
    // we can start creating the line as soon as we have some references
    if (el) {
      this.groupRef = el;
      this.makeScales();
      this.d3SelectElement();
    }
  }

  d3SelectElement() {
    // select the group reference to generate a d3 object
    if (this.groupRef) this.svg = select(this.groupRef);
  }

  // construct the line generator
  d3GenerateLine(props = this.props) {
    const { data } = props;
    // stored scales
    // shared between line and its extended class(es) to sync scaling behaviors
    const { x, y } = this.state;

    const vals = data.map(d => d.x);

    const d3Line = line(data)
      // value formatters
      .x(d => x(vals.indexOf(d.x)))
      .y(d => y(d.y))
      // smooth the curve without changing known data point values
      .curve(curveCardinal);

    // store the d3 line generator
    this.setState({
      line: d3Line,
    });
  }

  // construct the scaling generators
  makeScales(props = this.props) {
    const { data, width, height, yRange, xRange, scaleType, yDomain } = props;

    // override or set the axis ranges
    const rangeX = xRange || [0, width];
    const rangeY = yRange || [height, 0];

    // make a generator for this x-scale
    const scaleFunction = generateScaleOfType(scaleType);
    // get the domain values for this x-scale
    const xDomain = generateXDomain(data, scaleType);

    // domain: a range reference for data inputs, scaled to the accompanying numeric range
    const x = scaleFunction()
      .domain(xDomain)
      .range(rangeX);

    // it's simple to start with a 0-100 range on the y-axis in general, as this works for
    // our scores directly, and most other things convert to percentages without visual loss
    const y = scaleLinear()
      .domain(yDomain)
      .range(rangeY);

    // store the scalers
    this.setState({
      x,
      y,
    }, () => {
      // we have the scalers, now make the line
      this.d3GenerateLine(props);
    });
  }

  // create the visible svg line element, calling the line generator as path data
  makeLine() {
    const { data, strokeWidth, color } = this.props;

    if (this.state.line) {
      return (
        <path
          fill="none"
          strokeWidth={strokeWidth}
          stroke={color}
          d={this.state.line.call(this, data)} />
      );
    }

    return null;
  }

  /* eslint-disable class-methods-use-this */
  makeSymbols() {
    // overridable method in extended class(es)
    return null;
  }
  /* eslint-enable class-methods-use-this */

  render() {
    const {
      gridClasses,
      className,
      style,
    } = this.props;

    const classes = classie([gridClasses, className]);

    const svgLine = this.makeLine();
    const svgSymbols = this.makeSymbols();

    return (
      <g
        className={classes}
        style={style}
        ref={this.setGroupRef}>
        {svgLine}
        {svgSymbols}
      </g>
    );
  }
}

D3Line.propTypes = propTypes;

D3Line.defaultProps = defaultProps;

export default D3Line;