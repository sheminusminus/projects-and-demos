## a small grab bag

this is a mix of some of the things i really enjoyed building and/or like the outcome of :) 

- some pieces of code that currently exist in a larger app, but are functionally independent

- a couple 10 second or less, related video demos

these are from products/projects i contribute to that have private repositories. what i've included are bits that i'm especially fond of and that i have permission to share.
