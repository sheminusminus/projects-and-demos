import { call, fork, put, take, all } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';

import {
  PubSubService,
  PubSubConstants,
  SocketService,
  SocketConstants,
} from '../../services';

import * as actions from './actions';
import * as api from './api';
import * as constants from './constants';
import {
  loginSuccess as userLoginSuccess,
  signupSuccess as userSignupSuccess,
} from '../user/actions';

import { SignupFormData } from './types';

const { MIXPANEL_EVENTS } = PubSubConstants.Topics;
const { connection } = SocketConstants.Events;

/**
 * socketEventChannel
 * Creates an event channel to listen for socket emissions and trigger redux
 * behaviors in response.
 * @param token {string} Authentication token
 * @returns {Channel<any>}
 */
async function socketEventChannel(token: string) {
  return eventChannel((emitter) => {
    SocketService.setAuthToken(token);

    return () => {};
  });
}

export function* requestLogin(
  email: string,
  password:string,
  deviceName: string,
  deviceUid: string,
) {
  try {
    const response = yield call(
      api.requestLogin,
      email,
      password,
      deviceName,
      deviceUid,
    );

    if (response.error) {
      yield put(actions.loginFailure(response.error.message));
    } else {
      const { data } = response.data;

      PubSubService.publish(MIXPANEL_EVENTS, {
        eventName: 'User login',
        properties: { userId: data.id },
      });

      yield all([
        put(actions.loginSuccess(data.sessionToken, data.refreshToken)),
        put(userLoginSuccess(data)),
      ]);
    }
  } catch (error) {
    yield put(actions.loginFailure(error.message));
  }
}

export function* requestLogout() {
  try {
    const response = yield call(api.requestLogout);

    if (response.error) {
      yield put(actions.logoutFailure(response.error.message));
    } else {
      PubSubService.publish(MIXPANEL_EVENTS, {
        eventName: 'User logout',
      });

      yield put(actions.logoutSuccess());
    }
  } catch (error) {
    yield put(actions.logoutFailure(error.message));
  }
}

export function* requestSignup(signupFormData: SignupFormData) {
  try {
    const response = yield call(api.requestSignup, signupFormData);

    if (response.error) {
      yield put(actions.signupFailure(response.error.message));
    } else {
      const { data } = response.data;

      PubSubService.publish(MIXPANEL_EVENTS, {
        eventName: 'User signup',
        properties: { userId: data.id },
      });

      yield all([
        put(actions.signupSuccess(data.sessionToken, data.refreshToken)),
        put(userSignupSuccess(data)),
      ]);
    }
  } catch (error) {
    yield put(actions.signupFailure(error.message));
  }
}

/**
 * Generator function to listen for externally emitted events, and
 * dispatch actions accordingly.
 * @returns {IterableIterator<*>}
 */
export function* eventWatch() {
  // yield to receiving a login success action, then continue after
  const { payload: authPayload } = yield take([
    constants.LOGIN_SUCCESS,
    constants.SIGNUP_SUCCESS,
  ]);

  const channel = yield call(socketEventChannel, authPayload.token);

  try {
    while (true) {
      const { type, payload = {} } = yield take(channel);

      switch (type) {
        default:
          yield null;
          break;
      }
    }
  } finally {
    // only called if we `emitter(END)`, terminating the block
    yield null;
  }
}

/**
 *  Generator function to listen for redux actions
 *
 *  Handles any action api requests as non-blocking calls and
 *    returns the appropriate action responses
 */
function* watch() {
  while (true) {
    const { type, payload = {} } = yield take([
      constants.LOGIN_REQUEST,
      constants.LOGOUT_REQUEST,
      constants.SIGNUP_REQUEST,
    ]);

    switch (type) {
      case constants.LOGIN_REQUEST:
        yield fork(
          requestLogin,
          payload.email,
          payload.password,
          payload.deviceName,
          payload.deviceUid,
        );
        break;

      case constants.LOGOUT_REQUEST:
        yield fork(requestLogout);
        break;

      case constants.SIGNUP_REQUEST:
        yield fork(requestSignup, payload.signupFormData);
        break;

      default:
        yield null;
    }
  }
}

export default function* rootSaga() {
  yield all([eventWatch(), watch()]);
} 